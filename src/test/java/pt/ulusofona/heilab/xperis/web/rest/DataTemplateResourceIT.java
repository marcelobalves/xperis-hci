package pt.ulusofona.heilab.xperis.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import pt.ulusofona.heilab.xperis.IntegrationTest;
import pt.ulusofona.heilab.xperis.domain.DataMimeType;
import pt.ulusofona.heilab.xperis.domain.DataTemplate;
import pt.ulusofona.heilab.xperis.repository.DataTemplateRepository;

/**
 * Integration tests for the {@link DataTemplateResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DataTemplateResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_STRUCTURE = "AAAAAAAAAA";
    private static final String UPDATED_DATA_STRUCTURE = "BBBBBBBBBB";

    private static final String DEFAULT_VIEWER_CLASSES = "AAAAAAAAAA";
    private static final String UPDATED_VIEWER_CLASSES = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/data-templates";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private DataTemplateRepository dataTemplateRepository;

    @Autowired
    private MockMvc restDataTemplateMockMvc;

    private DataTemplate dataTemplate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataTemplate createEntity() {
        DataTemplate dataTemplate = new DataTemplate()
            .name(DEFAULT_NAME)
            .dataStructure(DEFAULT_DATA_STRUCTURE)
            .viewerClasses(DEFAULT_VIEWER_CLASSES);
        // Add required entity
        DataMimeType dataMimeType;
        dataMimeType = pt.ulusofona.heilab.xperis.web.rest.DataMimeTypeResourceIT.createEntity();
        dataMimeType.setId("fixed-id-for-tests");
        dataTemplate.setDataMimeType(dataMimeType);
        return dataTemplate;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataTemplate createUpdatedEntity() {
        DataTemplate dataTemplate = new DataTemplate()
            .name(UPDATED_NAME)
            .dataStructure(UPDATED_DATA_STRUCTURE)
            .viewerClasses(UPDATED_VIEWER_CLASSES);
        // Add required entity
        DataMimeType dataMimeType;
        dataMimeType = pt.ulusofona.heilab.xperis.web.rest.DataMimeTypeResourceIT.createUpdatedEntity();
        dataMimeType.setId("fixed-id-for-tests");
        dataTemplate.setDataMimeType(dataMimeType);
        return dataTemplate;
    }

    @BeforeEach
    public void initTest() {
        dataTemplateRepository.deleteAll();
        dataTemplate = createEntity();
    }

    @Test
    void createDataTemplate() throws Exception {
        int databaseSizeBeforeCreate = dataTemplateRepository.findAll().size();
        // Create the DataTemplate
        restDataTemplateMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dataTemplate)))
            .andExpect(status().isCreated());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        DataTemplate testDataTemplate = dataTemplateList.get(dataTemplateList.size() - 1);
        assertThat(testDataTemplate.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDataTemplate.getDataStructure()).isEqualTo(DEFAULT_DATA_STRUCTURE);
        assertThat(testDataTemplate.getViewerClasses()).isEqualTo(DEFAULT_VIEWER_CLASSES);
    }

    @Test
    void createDataTemplateWithExistingId() throws Exception {
        // Create the DataTemplate with an existing ID
        dataTemplate.setId("existing_id");

        int databaseSizeBeforeCreate = dataTemplateRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataTemplateMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dataTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataTemplateRepository.findAll().size();
        // set the field null
        dataTemplate.setName(null);

        // Create the DataTemplate, which fails.

        restDataTemplateMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dataTemplate)))
            .andExpect(status().isBadRequest());

        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllDataTemplates() throws Exception {
        // Initialize the database
        dataTemplateRepository.save(dataTemplate);

        // Get all the dataTemplateList
        restDataTemplateMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataTemplate.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].dataStructure").value(hasItem(DEFAULT_DATA_STRUCTURE.toString())))
            .andExpect(jsonPath("$.[*].viewerClasses").value(hasItem(DEFAULT_VIEWER_CLASSES)));
    }

    @Test
    void getDataTemplate() throws Exception {
        // Initialize the database
        dataTemplateRepository.save(dataTemplate);

        // Get the dataTemplate
        restDataTemplateMockMvc
            .perform(get(ENTITY_API_URL_ID, dataTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dataTemplate.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.dataStructure").value(DEFAULT_DATA_STRUCTURE.toString()))
            .andExpect(jsonPath("$.viewerClasses").value(DEFAULT_VIEWER_CLASSES));
    }

    @Test
    void getNonExistingDataTemplate() throws Exception {
        // Get the dataTemplate
        restDataTemplateMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    void putNewDataTemplate() throws Exception {
        // Initialize the database
        dataTemplateRepository.save(dataTemplate);

        int databaseSizeBeforeUpdate = dataTemplateRepository.findAll().size();

        // Update the dataTemplate
        DataTemplate updatedDataTemplate = dataTemplateRepository.findById(dataTemplate.getId()).get();
        updatedDataTemplate.name(UPDATED_NAME).dataStructure(UPDATED_DATA_STRUCTURE).viewerClasses(UPDATED_VIEWER_CLASSES);

        restDataTemplateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDataTemplate.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDataTemplate))
            )
            .andExpect(status().isOk());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeUpdate);
        DataTemplate testDataTemplate = dataTemplateList.get(dataTemplateList.size() - 1);
        assertThat(testDataTemplate.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDataTemplate.getDataStructure()).isEqualTo(UPDATED_DATA_STRUCTURE);
        assertThat(testDataTemplate.getViewerClasses()).isEqualTo(UPDATED_VIEWER_CLASSES);
    }

    @Test
    void putNonExistingDataTemplate() throws Exception {
        int databaseSizeBeforeUpdate = dataTemplateRepository.findAll().size();
        dataTemplate.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDataTemplateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dataTemplate.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dataTemplate))
            )
            .andExpect(status().isBadRequest());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchDataTemplate() throws Exception {
        int databaseSizeBeforeUpdate = dataTemplateRepository.findAll().size();
        dataTemplate.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDataTemplateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dataTemplate))
            )
            .andExpect(status().isBadRequest());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamDataTemplate() throws Exception {
        int databaseSizeBeforeUpdate = dataTemplateRepository.findAll().size();
        dataTemplate.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDataTemplateMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dataTemplate)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateDataTemplateWithPatch() throws Exception {
        // Initialize the database
        dataTemplateRepository.save(dataTemplate);

        int databaseSizeBeforeUpdate = dataTemplateRepository.findAll().size();

        // Update the dataTemplate using partial update
        DataTemplate partialUpdatedDataTemplate = new DataTemplate();
        partialUpdatedDataTemplate.setId(dataTemplate.getId());

        partialUpdatedDataTemplate.name(UPDATED_NAME);

        restDataTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDataTemplate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDataTemplate))
            )
            .andExpect(status().isOk());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeUpdate);
        DataTemplate testDataTemplate = dataTemplateList.get(dataTemplateList.size() - 1);
        assertThat(testDataTemplate.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDataTemplate.getDataStructure()).isEqualTo(DEFAULT_DATA_STRUCTURE);
        assertThat(testDataTemplate.getViewerClasses()).isEqualTo(DEFAULT_VIEWER_CLASSES);
    }

    @Test
    void fullUpdateDataTemplateWithPatch() throws Exception {
        // Initialize the database
        dataTemplateRepository.save(dataTemplate);

        int databaseSizeBeforeUpdate = dataTemplateRepository.findAll().size();

        // Update the dataTemplate using partial update
        DataTemplate partialUpdatedDataTemplate = new DataTemplate();
        partialUpdatedDataTemplate.setId(dataTemplate.getId());

        partialUpdatedDataTemplate.name(UPDATED_NAME).dataStructure(UPDATED_DATA_STRUCTURE).viewerClasses(UPDATED_VIEWER_CLASSES);

        restDataTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDataTemplate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDataTemplate))
            )
            .andExpect(status().isOk());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeUpdate);
        DataTemplate testDataTemplate = dataTemplateList.get(dataTemplateList.size() - 1);
        assertThat(testDataTemplate.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDataTemplate.getDataStructure()).isEqualTo(UPDATED_DATA_STRUCTURE);
        assertThat(testDataTemplate.getViewerClasses()).isEqualTo(UPDATED_VIEWER_CLASSES);
    }

    @Test
    void patchNonExistingDataTemplate() throws Exception {
        int databaseSizeBeforeUpdate = dataTemplateRepository.findAll().size();
        dataTemplate.setId(UUID.randomUUID().toString());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDataTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dataTemplate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dataTemplate))
            )
            .andExpect(status().isBadRequest());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchDataTemplate() throws Exception {
        int databaseSizeBeforeUpdate = dataTemplateRepository.findAll().size();
        dataTemplate.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDataTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dataTemplate))
            )
            .andExpect(status().isBadRequest());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamDataTemplate() throws Exception {
        int databaseSizeBeforeUpdate = dataTemplateRepository.findAll().size();
        dataTemplate.setId(UUID.randomUUID().toString());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDataTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dataTemplate))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DataTemplate in the database
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteDataTemplate() throws Exception {
        // Initialize the database
        dataTemplateRepository.save(dataTemplate);

        int databaseSizeBeforeDelete = dataTemplateRepository.findAll().size();

        // Delete the dataTemplate
        restDataTemplateMockMvc
            .perform(delete(ENTITY_API_URL_ID, dataTemplate.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DataTemplate> dataTemplateList = dataTemplateRepository.findAll();
        assertThat(dataTemplateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
