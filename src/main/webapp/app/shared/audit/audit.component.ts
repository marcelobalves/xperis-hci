import { Component, Input } from '@angular/core';

@Component({
  selector: 'jhi-audit',
  templateUrl: './audit.component.html',
})
export class AuditComponent {
  @Input() entity: any;
}
