import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { Plugin, IPlugin } from '../plugin.model';
import { PluginService } from '../service/plugin.service';
import { EventManager } from 'app/core/util/event-manager.service';
import { DataUtils } from 'app/core/util/data-util.service';
import { IDataMimeType } from 'app/entities/data-mime-type/data-mime-type.model';
import { DataMimeTypeService } from 'app/entities/data-mime-type/service/data-mime-type.service';

@Component({
  selector: 'jhi-plugin-update',
  templateUrl: './plugin-update.component.html',
})
export class PluginUpdateComponent implements OnInit {
  isSaving = false;

  dataMimeTypesSharedCollection: IDataMimeType[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    className: [null, [Validators.required]],
    fileName: [],
    dataMimeType: [null, Validators.required],
    activated: [],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected pluginService: PluginService,
    protected dataMimeTypeService: DataMimeTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ plugin }) => {
      this.updateForm(plugin);

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const plugin = this.createFromForm();
    if (plugin.id !== undefined) {
      this.subscribeToSaveResponse(this.pluginService.partialUpdate(plugin));
    } else {
      this.subscribeToSaveResponse(this.pluginService.create(plugin));
    }
  }

  trackDataMimeTypeById(index: number, item: IDataMimeType): string {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPlugin>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(plugin: IPlugin): void {
    this.editForm.patchValue({
      id: plugin.id,
      name: plugin.name,
      className: plugin.className,
      fileName: plugin.fileName,
      dataMimeType: plugin.dataMimeType,
      activated: plugin.activated,
    });

    this.dataMimeTypesSharedCollection = this.dataMimeTypeService.addDataMimeTypeToCollectionIfMissing(
      this.dataMimeTypesSharedCollection,
      plugin.dataMimeType
    );
  }

  protected loadRelationshipsOptions(): void {
    this.dataMimeTypeService
      .queryAll()
      .pipe(map((res: HttpResponse<IDataMimeType[]>) => res.body ?? []))
      .pipe(
        map((dataMimeTypes: IDataMimeType[]) =>
          this.dataMimeTypeService.addDataMimeTypeToCollectionIfMissing(dataMimeTypes, this.editForm.get('dataMimeType')!.value)
        )
      )
      .subscribe((dataMimeTypes: IDataMimeType[]) => (this.dataMimeTypesSharedCollection = dataMimeTypes));
  }

  protected createFromForm(): IPlugin {
    return {
      ...new Plugin(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      className: this.editForm.get(['className'])!.value,
      fileName: this.editForm.get(['fileName'])!.value,
      dataMimeType: this.editForm.get(['dataMimeType'])!.value,
      activated: this.editForm.get(['activated'])!.value,
    };
  }
}
