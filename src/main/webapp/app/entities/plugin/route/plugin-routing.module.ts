import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PluginComponent } from '../list/plugin.component';
import { PluginDetailComponent } from '../detail/plugin-detail.component';
import { PluginUpdateComponent } from '../update/plugin-update.component';
import { PluginRoutingResolveService } from './plugin-routing-resolve.service';

const pluginRoute: Routes = [
  {
    path: '',
    component: PluginComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PluginDetailComponent,
    resolve: {
      plugin: PluginRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PluginUpdateComponent,
    resolve: {
      plugin: PluginRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PluginUpdateComponent,
    resolve: {
      plugin: PluginRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(pluginRoute)],
  exports: [RouterModule],
})
export class PluginRoutingModule {}
