import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ExperimentMetaDataComponent } from '../list/experiment-meta-data.component';
import { ExperimentMetaDataDetailComponent } from '../detail/experiment-meta-data-detail.component';
import { ExperimentMetaDataUpdateComponent } from '../update/experiment-meta-data-update.component';
import { ExperimentMetaDataRoutingResolveService } from './experiment-meta-data-routing-resolve.service';

const experimentMetaDataRoute: Routes = [
  {
    path: '',
    component: ExperimentMetaDataComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ExperimentMetaDataDetailComponent,
    resolve: {
      experimentMetaData: ExperimentMetaDataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ExperimentMetaDataUpdateComponent,
    resolve: {
      experimentMetaData: ExperimentMetaDataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ExperimentMetaDataUpdateComponent,
    resolve: {
      experimentMetaData: ExperimentMetaDataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(experimentMetaDataRoute)],
  exports: [RouterModule],
})
export class ExperimentMetaDataRoutingModule {}
