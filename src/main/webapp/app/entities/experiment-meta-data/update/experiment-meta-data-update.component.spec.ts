jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ExperimentMetaDataService } from '../service/experiment-meta-data.service';
import { IExperimentMetaData, ExperimentMetaData } from '../experiment-meta-data.model';
import { IExperiment } from 'app/entities/experiment/experiment.model';
import { ExperimentService } from 'app/entities/experiment/service/experiment.service';
import { IDataMimeType } from 'app/entities/data-mime-type/data-mime-type.model';
import { DataMimeTypeService } from 'app/entities/data-mime-type/service/data-mime-type.service';

import { ExperimentMetaDataUpdateComponent } from './experiment-meta-data-update.component';

describe('Component Tests', () => {
  describe('ExperimentMetaData Management Update Component', () => {
    let comp: ExperimentMetaDataUpdateComponent;
    let fixture: ComponentFixture<ExperimentMetaDataUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let experimentMetaDataService: ExperimentMetaDataService;
    let experimentService: ExperimentService;
    let dataMimeTypeService: DataMimeTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ExperimentMetaDataUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ExperimentMetaDataUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExperimentMetaDataUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      experimentMetaDataService = TestBed.inject(ExperimentMetaDataService);
      experimentService = TestBed.inject(ExperimentService);
      dataMimeTypeService = TestBed.inject(DataMimeTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Experiment query and add missing value', () => {
        const experimentMetaData: IExperimentMetaData = { id: 'CBA' };
        const experiment: IExperiment = { id: 'Tugrik Mauritania' };
        experimentMetaData.experiment = experiment;

        const experimentCollection: IExperiment[] = [{ id: 'Germany Pants' }];
        spyOn(experimentService, 'query').and.returnValue(of(new HttpResponse({ body: experimentCollection })));
        const additionalExperiments = [experiment];
        const expectedCollection: IExperiment[] = [...additionalExperiments, ...experimentCollection];
        spyOn(experimentService, 'addExperimentToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ experimentMetaData });
        comp.ngOnInit();

        expect(experimentService.query).toHaveBeenCalled();
        expect(experimentService.addExperimentToCollectionIfMissing).toHaveBeenCalledWith(experimentCollection, ...additionalExperiments);
        expect(comp.experimentsSharedCollection).toEqual(expectedCollection);
      });

      it('Should call DataMimeType query and add missing value', () => {
        const experimentMetaData: IExperimentMetaData = { id: 'CBA' };
        const dataMimeType: IDataMimeType = { id: 'Principal' };
        experimentMetaData.dataMimeType = dataMimeType;

        const dataMimeTypeCollection: IDataMimeType[] = [{ id: 'standardization' }];
        spyOn(dataMimeTypeService, 'query').and.returnValue(of(new HttpResponse({ body: dataMimeTypeCollection })));
        const additionalDataMimeTypes = [dataMimeType];
        const expectedCollection: IDataMimeType[] = [...additionalDataMimeTypes, ...dataMimeTypeCollection];
        spyOn(dataMimeTypeService, 'addDataMimeTypeToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ experimentMetaData });
        comp.ngOnInit();

        expect(dataMimeTypeService.query).toHaveBeenCalled();
        expect(dataMimeTypeService.addDataMimeTypeToCollectionIfMissing).toHaveBeenCalledWith(
          dataMimeTypeCollection,
          ...additionalDataMimeTypes
        );
        expect(comp.dataMimeTypesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const experimentMetaData: IExperimentMetaData = { id: 'CBA' };
        const experiment: IExperiment = { id: 'Mobility back-end New' };
        experimentMetaData.experiment = experiment;
        const dataMimeType: IDataMimeType = { id: 'Virginia alarm' };
        experimentMetaData.dataMimeType = dataMimeType;

        activatedRoute.data = of({ experimentMetaData });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(experimentMetaData));
        expect(comp.experimentsSharedCollection).toContain(experiment);
        expect(comp.dataMimeTypesSharedCollection).toContain(dataMimeType);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const experimentMetaData = { id: 'ABC' };
        spyOn(experimentMetaDataService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ experimentMetaData });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: experimentMetaData }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(experimentMetaDataService.update).toHaveBeenCalledWith(experimentMetaData);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const experimentMetaData = new ExperimentMetaData();
        spyOn(experimentMetaDataService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ experimentMetaData });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: experimentMetaData }));
        saveSubject.complete();

        // THEN
        expect(experimentMetaDataService.create).toHaveBeenCalledWith(experimentMetaData);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const experimentMetaData = { id: 'ABC' };
        spyOn(experimentMetaDataService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ experimentMetaData });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(experimentMetaDataService.update).toHaveBeenCalledWith(experimentMetaData);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackExperimentById', () => {
        it('Should return tracked Experiment primary key', () => {
          const entity = { id: 'ABC' };
          const trackResult = comp.trackExperimentById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackDataMimeTypeById', () => {
        it('Should return tracked DataMimeType primary key', () => {
          const entity = { id: 'ABC' };
          const trackResult = comp.trackDataMimeTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
