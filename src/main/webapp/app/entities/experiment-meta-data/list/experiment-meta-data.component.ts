import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IExperimentMetaData } from '../experiment-meta-data.model';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { ExperimentMetaDataService } from '../service/experiment-meta-data.service';
import { ExperimentMetaDataDeleteDialogComponent } from '../delete/experiment-meta-data-delete-dialog.component';
import { DataUtils } from 'app/core/util/data-util.service';
import { PluginService } from 'app/entities/plugin/service/plugin.service';
import { IPlugin } from 'app/entities/plugin/plugin.model';

@Component({
  selector: 'jhi-experiment-meta-data',
  templateUrl: './experiment-meta-data.component.html',
})
export class ExperimentMetaDataComponent implements OnInit {
  experimentMetaData?: IExperimentMetaData[];
  plugin?: IPlugin[];
  isLoading = false;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page?: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected experimentMetaDataService: ExperimentMetaDataService,
    protected pluginService: PluginService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: DataUtils,
    protected router: Router,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    this.isLoading = true;
    const pageToLoad: number = page ?? this.page ?? 1;

    this.experimentMetaDataService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IExperimentMetaData[]>) => {
          this.loadPlugins();
          this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate);
        },
        () => {
          this.isLoading = false;
          this.onError();
        }
      );
  }
  loadPlugins(): void {
    this.pluginService.queryAll().subscribe(
      (res: HttpResponse<IPlugin[]>) => {
        this.isLoading = false;
        this.plugin = res.body ?? [];
      },
      () => {
        this.isLoading = false;
        this.onError();
      }
    );
  }

  ngOnInit(): void {
    this.handleNavigation();
  }

  trackId(index: number, item: IExperimentMetaData): string {
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    return this.dataUtils.openFile(base64String, contentType);
  }

  delete(experimentMetaData: IExperimentMetaData): void {
    const modalRef = this.modalService.open(ExperimentMetaDataDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.experimentMetaData = experimentMetaData;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadPage();
      }
    });
  }

  downloadZip(id: string | undefined): void {
    if (id) {
      this.experimentMetaDataService.getZip(id).subscribe(res => {
        this.dataUtils.openFileBlob(res, 'application/zip', id);
      });
    }
  }

  getPluginView(id: string | undefined, plugin: IPlugin): void {
    if (id && plugin.name) {
      this.experimentMetaDataService.getView(id, plugin.name).subscribe(res => {
        this.dataUtils.openFileBlob(res, plugin.dataMimeType?.type, id);
      });
    }
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap]).subscribe(([data, params]) => {
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    });
  }

  protected onSuccess(data: IExperimentMetaData[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/experiment-meta-data'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.experimentMetaData = data ?? [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }
}
