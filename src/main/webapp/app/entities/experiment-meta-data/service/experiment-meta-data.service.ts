import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IExperimentMetaData, getExperimentMetaDataIdentifier } from '../experiment-meta-data.model';

export type EntityResponseType = HttpResponse<IExperimentMetaData>;
export type EntityArrayResponseType = HttpResponse<IExperimentMetaData[]>;

@Injectable({ providedIn: 'root' })
export class ExperimentMetaDataService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/experiment-meta-data');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(experimentMetaData: IExperimentMetaData): Observable<EntityResponseType> {
    return this.http.post<IExperimentMetaData>(this.resourceUrl, experimentMetaData, { observe: 'response' });
  }

  update(experimentMetaData: IExperimentMetaData): Observable<EntityResponseType> {
    return this.http.put<IExperimentMetaData>(
      `${this.resourceUrl}/${getExperimentMetaDataIdentifier(experimentMetaData) as string}`,
      experimentMetaData,
      { observe: 'response' }
    );
  }

  partialUpdate(experimentMetaData: IExperimentMetaData): Observable<EntityResponseType> {
    return this.http.patch<IExperimentMetaData>(
      `${this.resourceUrl}/${getExperimentMetaDataIdentifier(experimentMetaData) as string}`,
      experimentMetaData,
      { observe: 'response', headers: { 'Content-Type': 'application/merge-patch+json' } }
    );
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http.get<IExperimentMetaData>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IExperimentMetaData[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addExperimentMetaDataToCollectionIfMissing(
    experimentMetaDataCollection: IExperimentMetaData[],
    ...experimentMetaDataToCheck: (IExperimentMetaData | null | undefined)[]
  ): IExperimentMetaData[] {
    const experimentMetaData: IExperimentMetaData[] = experimentMetaDataToCheck.filter(isPresent);
    if (experimentMetaData.length > 0) {
      const experimentMetaDataCollectionIdentifiers = experimentMetaDataCollection.map(
        experimentMetaDataItem => getExperimentMetaDataIdentifier(experimentMetaDataItem)!
      );
      const experimentMetaDataToAdd = experimentMetaData.filter(experimentMetaDataItem => {
        const experimentMetaDataIdentifier = getExperimentMetaDataIdentifier(experimentMetaDataItem);
        if (experimentMetaDataIdentifier == null || experimentMetaDataCollectionIdentifiers.includes(experimentMetaDataIdentifier)) {
          return false;
        }
        experimentMetaDataCollectionIdentifiers.push(experimentMetaDataIdentifier);
        return true;
      });
      return [...experimentMetaDataToAdd, ...experimentMetaDataCollection];
    }
    return experimentMetaDataCollection;
  }

  getZip(id: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}/${id}/zip`, {
      headers: { Accept: '*/*', 'Accept-Encoding': 'gzip, deflate, br' },
      responseType: 'blob' as 'json',
    });
  }

  getView(id: string, className: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}/${id}/plugin/${className}`, {
      headers: { Accept: '*/*', 'Accept-Encoding': 'gzip, deflate, br' },
      responseType: 'blob' as 'json',
    });
  }
}
