import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { DataTemplateComponent } from './list/data-template.component';
import { DataTemplateDetailComponent } from './detail/data-template-detail.component';
import { DataTemplateUpdateComponent } from './update/data-template-update.component';
import { DataTemplateDeleteDialogComponent } from './delete/data-template-delete-dialog.component';
import { DataTemplateRoutingModule } from './route/data-template-routing.module';

@NgModule({
  imports: [SharedModule, DataTemplateRoutingModule],
  declarations: [DataTemplateComponent, DataTemplateDetailComponent, DataTemplateUpdateComponent, DataTemplateDeleteDialogComponent],
  entryComponents: [DataTemplateDeleteDialogComponent],
})
export class DataTemplateModule {}
