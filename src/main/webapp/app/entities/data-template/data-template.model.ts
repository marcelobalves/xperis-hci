import { IDataMimeType } from 'app/entities/data-mime-type/data-mime-type.model';

export interface IDataTemplate {
  id?: string;
  name?: string;
  dataStructure?: string;
  viewerClasses?: string | null;
  dataMimeType?: IDataMimeType;
}

export class DataTemplate implements IDataTemplate {
  constructor(
    public id?: string,
    public name?: string,
    public dataStructure?: string,
    public viewerClasses?: string | null,
    public dataMimeType?: IDataMimeType
  ) {}
}

export function getDataTemplateIdentifier(dataTemplate: IDataTemplate): string | undefined {
  return dataTemplate.id;
}
