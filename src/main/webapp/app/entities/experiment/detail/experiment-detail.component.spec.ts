import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ExperimentDetailComponent } from './experiment-detail.component';

describe('Component Tests', () => {
  describe('Experiment Management Detail Component', () => {
    let comp: ExperimentDetailComponent;
    let fixture: ComponentFixture<ExperimentDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ExperimentDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ experiment: { id: 'ABC' } }) },
          },
        ],
      })
        .overrideTemplate(ExperimentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExperimentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load experiment on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.experiment).toEqual(jasmine.objectContaining({ id: 'ABC' }));
      });
    });
  });
});
