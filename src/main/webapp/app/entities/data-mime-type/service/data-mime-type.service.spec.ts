import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDataMimeType, DataMimeType } from '../data-mime-type.model';

import { DataMimeTypeService } from './data-mime-type.service';

describe('Service Tests', () => {
  describe('DataMimeType Service', () => {
    let service: DataMimeTypeService;
    let httpMock: HttpTestingController;
    let elemDefault: IDataMimeType;
    let expectedResult: IDataMimeType | IDataMimeType[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(DataMimeTypeService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 'AAAAAAA',
        type: 'AAAAAAA',
        fileExtension: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('ABC').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a DataMimeType', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new DataMimeType()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a DataMimeType', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
            fileExtension: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a DataMimeType', () => {
        const patchObject = Object.assign(
          {
            fileExtension: 'BBBBBB',
          },
          new DataMimeType()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of DataMimeType', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            name: 'BBBBBB',
            fileExtension: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a DataMimeType', () => {
        service.delete('ABC').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addDataMimeTypeToCollectionIfMissing', () => {
        it('should add a DataMimeType to an empty array', () => {
          const dataMimeType: IDataMimeType = { id: 'ABC' };
          expectedResult = service.addDataMimeTypeToCollectionIfMissing([], dataMimeType);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dataMimeType);
        });

        it('should not add a DataMimeType to an array that contains it', () => {
          const dataMimeType: IDataMimeType = { id: 'ABC' };
          const dataMimeTypeCollection: IDataMimeType[] = [
            {
              ...dataMimeType,
            },
            { id: 'CBA' },
          ];
          expectedResult = service.addDataMimeTypeToCollectionIfMissing(dataMimeTypeCollection, dataMimeType);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a DataMimeType to an array that doesn't contain it", () => {
          const dataMimeType: IDataMimeType = { id: 'ABC' };
          const dataMimeTypeCollection: IDataMimeType[] = [{ id: 'CBA' }];
          expectedResult = service.addDataMimeTypeToCollectionIfMissing(dataMimeTypeCollection, dataMimeType);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dataMimeType);
        });

        it('should add only unique DataMimeType to an array', () => {
          const dataMimeTypeArray: IDataMimeType[] = [{ id: 'ABC' }, { id: 'CBA' }, { id: 'generate' }];
          const dataMimeTypeCollection: IDataMimeType[] = [{ id: 'ABC' }];
          expectedResult = service.addDataMimeTypeToCollectionIfMissing(dataMimeTypeCollection, ...dataMimeTypeArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const dataMimeType: IDataMimeType = { id: 'ABC' };
          const dataMimeType2: IDataMimeType = { id: 'CBA' };
          expectedResult = service.addDataMimeTypeToCollectionIfMissing([], dataMimeType, dataMimeType2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(dataMimeType);
          expect(expectedResult).toContain(dataMimeType2);
        });

        it('should accept null and undefined values', () => {
          const dataMimeType: IDataMimeType = { id: 'ABC' };
          expectedResult = service.addDataMimeTypeToCollectionIfMissing([], null, dataMimeType, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(dataMimeType);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
