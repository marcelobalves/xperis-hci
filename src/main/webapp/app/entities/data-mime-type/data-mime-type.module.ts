import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { DataMimeTypeComponent } from './list/data-mime-type.component';
import { DataMimeTypeDetailComponent } from './detail/data-mime-type-detail.component';
import { DataMimeTypeUpdateComponent } from './update/data-mime-type-update.component';
import { DataMimeTypeDeleteDialogComponent } from './delete/data-mime-type-delete-dialog.component';
import { DataMimeTypeRoutingModule } from './route/data-mime-type-routing.module';

@NgModule({
  imports: [SharedModule, DataMimeTypeRoutingModule],
  declarations: [DataMimeTypeComponent, DataMimeTypeDetailComponent, DataMimeTypeUpdateComponent, DataMimeTypeDeleteDialogComponent],
  entryComponents: [DataMimeTypeDeleteDialogComponent],
})
export class DataMimeTypeModule {}
