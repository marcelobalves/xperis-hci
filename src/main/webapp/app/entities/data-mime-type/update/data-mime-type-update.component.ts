import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IDataMimeType, DataMimeType } from '../data-mime-type.model';
import { DataMimeTypeService } from '../service/data-mime-type.service';

@Component({
  selector: 'jhi-data-mime-type-update',
  templateUrl: './data-mime-type-update.component.html',
})
export class DataMimeTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    type: [null, [Validators.required]],
    description: [],
    fileExtension: [null, [Validators.required]],
  });

  constructor(protected dataMimeTypeService: DataMimeTypeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ dataMimeType }) => {
      this.updateForm(dataMimeType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const dataMimeType = this.createFromForm();
    if (dataMimeType.id !== undefined) {
      this.subscribeToSaveResponse(this.dataMimeTypeService.partialUpdate(dataMimeType));
    } else {
      this.subscribeToSaveResponse(this.dataMimeTypeService.create(dataMimeType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDataMimeType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(dataMimeType: IDataMimeType): void {
    this.editForm.patchValue({
      id: dataMimeType.id,
      type: dataMimeType.type,
      description: dataMimeType.description,
      fileExtension: dataMimeType.fileExtension,
    });
  }

  protected createFromForm(): IDataMimeType {
    return {
      ...new DataMimeType(),
      id: this.editForm.get(['id'])!.value,
      type: this.editForm.get(['type'])!.value,
      description: this.editForm.get(['description'])!.value,
      fileExtension: this.editForm.get(['fileExtension'])!.value,
    };
  }
}
