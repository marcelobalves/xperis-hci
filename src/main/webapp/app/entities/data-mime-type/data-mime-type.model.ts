import { IExperimentMetaData } from 'app/entities/experiment-meta-data/experiment-meta-data.model';
import { IDataTemplate } from 'app/entities/data-template/data-template.model';

export interface IDataMimeType {
  id?: string;
  type?: string;
  description?: string;
  fileExtension?: string;
  experimentMetaData?: IExperimentMetaData[] | null;
  dataTemplates?: IDataTemplate[] | null;
}

export class DataMimeType implements IDataMimeType {
  constructor(
    public id?: string,
    public type?: string,
    public description?: string,
    public fileExtension?: string,
    public experimentMetaData?: IExperimentMetaData[] | null,
    public dataTemplates?: IDataTemplate[] | null
  ) {}
}

export function getDataMimeTypeIdentifier(dataMimeType: IDataMimeType): string | undefined {
  return dataMimeType.id;
}
