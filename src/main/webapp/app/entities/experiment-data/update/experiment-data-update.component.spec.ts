jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ExperimentDataService } from '../service/experiment-data.service';
import { IExperimentData, ExperimentData } from '../experiment-data.model';
import { IExperimentMetaData } from 'app/entities/experiment-meta-data/experiment-meta-data.model';
import { ExperimentMetaDataService } from 'app/entities/experiment-meta-data/service/experiment-meta-data.service';

import { ExperimentDataUpdateComponent } from './experiment-data-update.component';

describe('Component Tests', () => {
  describe('ExperimentData Management Update Component', () => {
    let comp: ExperimentDataUpdateComponent;
    let fixture: ComponentFixture<ExperimentDataUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let experimentDataService: ExperimentDataService;
    let experimentMetaDataService: ExperimentMetaDataService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ExperimentDataUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ExperimentDataUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExperimentDataUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      experimentDataService = TestBed.inject(ExperimentDataService);
      experimentMetaDataService = TestBed.inject(ExperimentMetaDataService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call ExperimentMetaData query and add missing value', () => {
        const experimentData: IExperimentData = { id: 'CBA' };
        const experimentMetaData: IExperimentMetaData = { id: 'Analyst Car web' };
        experimentData.experimentMetaData = experimentMetaData;

        const experimentMetaDataCollection: IExperimentMetaData[] = [{ id: 'Engineer Rhode' }];
        spyOn(experimentMetaDataService, 'query').and.returnValue(of(new HttpResponse({ body: experimentMetaDataCollection })));
        const additionalExperimentMetaData = [experimentMetaData];
        const expectedCollection: IExperimentMetaData[] = [...additionalExperimentMetaData, ...experimentMetaDataCollection];
        spyOn(experimentMetaDataService, 'addExperimentMetaDataToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ experimentData });
        comp.ngOnInit();

        expect(experimentMetaDataService.query).toHaveBeenCalled();
        expect(experimentMetaDataService.addExperimentMetaDataToCollectionIfMissing).toHaveBeenCalledWith(
          experimentMetaDataCollection,
          ...additionalExperimentMetaData
        );
        expect(comp.experimentMetaDataSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const experimentData: IExperimentData = { id: 'CBA' };
        const experimentMetaData: IExperimentMetaData = { id: 'JSON Indiana Curve' };
        experimentData.experimentMetaData = experimentMetaData;

        activatedRoute.data = of({ experimentData });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(experimentData));
        expect(comp.experimentMetaDataSharedCollection).toContain(experimentMetaData);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const experimentData = { id: 'ABC' };
        spyOn(experimentDataService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ experimentData });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: experimentData }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(experimentDataService.update).toHaveBeenCalledWith(experimentData);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const experimentData = new ExperimentData();
        spyOn(experimentDataService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ experimentData });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: experimentData }));
        saveSubject.complete();

        // THEN
        expect(experimentDataService.create).toHaveBeenCalledWith(experimentData);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const experimentData = { id: 'ABC' };
        spyOn(experimentDataService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ experimentData });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(experimentDataService.update).toHaveBeenCalledWith(experimentData);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackExperimentMetaDataById', () => {
        it('Should return tracked ExperimentMetaData primary key', () => {
          const entity = { id: 'ABC' };
          const trackResult = comp.trackExperimentMetaDataById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
