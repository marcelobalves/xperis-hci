import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DataUtils } from 'app/core/util/data-util.service';

import { ExperimentDataDetailComponent } from './experiment-data-detail.component';

describe('Component Tests', () => {
  describe('ExperimentData Management Detail Component', () => {
    let comp: ExperimentDataDetailComponent;
    let fixture: ComponentFixture<ExperimentDataDetailComponent>;
    let dataUtils: DataUtils;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ExperimentDataDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ experimentData: { id: 'ABC' } }) },
          },
        ],
      })
        .overrideTemplate(ExperimentDataDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExperimentDataDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = TestBed.inject(DataUtils);
    });

    describe('OnInit', () => {
      it('Should load experimentData on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.experimentData).toEqual(jasmine.objectContaining({ id: 'ABC' }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from DataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from DataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeBase64, fakeContentType);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeBase64, fakeContentType);
      });
    });
  });
});
