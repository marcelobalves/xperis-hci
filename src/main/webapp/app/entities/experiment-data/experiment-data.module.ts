import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { ExperimentDataComponent } from './list/experiment-data.component';
import { ExperimentDataDetailComponent } from './detail/experiment-data-detail.component';
import { ExperimentDataUpdateComponent } from './update/experiment-data-update.component';
import { ExperimentDataDeleteDialogComponent } from './delete/experiment-data-delete-dialog.component';
import { ExperimentDataRoutingModule } from './route/experiment-data-routing.module';

@NgModule({
  imports: [SharedModule, ExperimentDataRoutingModule],
  declarations: [
    ExperimentDataComponent,
    ExperimentDataDetailComponent,
    ExperimentDataUpdateComponent,
    ExperimentDataDeleteDialogComponent,
  ],
  entryComponents: [ExperimentDataDeleteDialogComponent],
})
export class ExperimentDataModule {}
