import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IExperimentData, ExperimentData } from '../experiment-data.model';
import { ExperimentDataService } from '../service/experiment-data.service';

@Injectable({ providedIn: 'root' })
export class ExperimentDataRoutingResolveService implements Resolve<IExperimentData> {
  constructor(protected service: ExperimentDataService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IExperimentData> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((experimentData: HttpResponse<ExperimentData>) => {
          if (experimentData.body) {
            return of(experimentData.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ExperimentData());
  }
}
