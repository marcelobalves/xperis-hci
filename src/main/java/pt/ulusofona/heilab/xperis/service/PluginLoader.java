package pt.ulusofona.heilab.xperis.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import pt.ulusofona.heilab.xperis.domain.Plugin;
import pt.ulusofona.heilab.xperis.service.spi.PluginViewer;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

@Component
public class PluginLoader {

    private final Logger log = LoggerFactory.getLogger(PluginLoader.class);

    private Map<String, Plugin> loadedPlugins;

    private final String FILE_EXTENSION = ".jar";

    private final PluginService pluginService;
    private final Environment env;

    public PluginLoader(PluginService pluginService, Environment env) {
        this.pluginService = pluginService;
        this.env = env;
        load();
    }

    public void load() {
        loadedPlugins = new HashMap<>();

        ServiceLoader<PluginViewer> systemServiceLoader = ServiceLoader.load(PluginViewer.class);
        systemServiceLoader.iterator().forEachRemaining(viewer -> loadedPlugins.put(viewer.getClass().getSimpleName(), getPlugin(viewer)));
        log.info("Plugins loaded from SystemLoader: {}", loadedPlugins.size());

        Path[] jarPaths = listJarFiles();
        URLClassLoader childClassLoader = new URLClassLoader(toURL(jarPaths), Thread.currentThread().getContextClassLoader());
        ServiceLoader<PluginViewer> jarServiceLoader = ServiceLoader.load(PluginViewer.class, childClassLoader);

        int i = 0;
        for (PluginViewer viewer : jarServiceLoader) {
            Plugin plugin = getPlugin(viewer);
            plugin.setFileName(jarPaths[i].getFileName().toString());
            loadedPlugins.put(plugin.getName(), plugin);
            i++;
        }
        log.info("Plugins loaded from JarFolder: {}", i);

        this.updateRepository();
    }

    private Plugin getPlugin(PluginViewer viewer) {
        Plugin plugin = new Plugin();
        plugin.setName(viewer.getClass().getSimpleName());
        plugin.setClassName(viewer.getClass().getCanonicalName());
        plugin.setViewer(viewer);
        return plugin;
    }

    public Set<String> getNames() {
        return loadedPlugins.keySet();
    }

    public Plugin getPlugin(String className) {
        return loadedPlugins.get(className);
    }

    public PluginViewer getViewer(String className) {
        Plugin info = getPlugin(className);
        if (info != null) return info.getViewer();
        return null;
    }

    private Path[] listJarFiles() {
        try {
            String folder = env.getProperty("xperis.conf-folder");
            log.info("Config Folder Configured (ENV): {}", folder);
            File configFolder = new File(folder);
            if (!configFolder.exists()) configFolder.mkdir();

            File pluginFolder = new File(folder + "/plugins");
            if (!pluginFolder.exists()) {
                pluginFolder.mkdir();
                return new Path[0];
            }

            Stream<Path> stream = Files.walk(pluginFolder.toPath(), 1);
            return stream
                .filter(file -> !Files.isDirectory(file))
                .filter(file -> file.getFileName().toString().endsWith(FILE_EXTENSION))
                .peek(file -> log.info("File read: {}", file.toString()))
                .toArray(Path[]::new);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private URL[] toURL(Path[] paths) {
        return Arrays
            .stream(paths)
            .map(
                p -> {
                    try {
                        return p.toUri().toURL();
                    } catch (MalformedURLException e) {
                        throw new RuntimeException(e.getMessage(), e);
                    }
                }
            )
            .toArray(URL[]::new);
    }

    /**
     * Update Plugins Repository with memory loaded plugins.
     */
    public void updateRepository() {
        List<Plugin> foundPlugins = new ArrayList<>();
        List<Plugin> dbPlugins = pluginService.findAll("");
        log.info("Plugins loaded from Database: {}", dbPlugins.size());

        Set<String> loaded = loadedPlugins.keySet();

        for (Plugin plugin : dbPlugins) {
            boolean found = loaded.stream().anyMatch(name -> plugin.getName().equals(name));
            if (found) {
                foundPlugins.add(plugin);
            } else {
                pluginService.delete(plugin.getId());
                log.info("Plugin deleted: {}", plugin.getName());
            }
        }

        for (String name : loaded) {
            boolean found = foundPlugins.stream().anyMatch(plugin -> plugin.getName().equals(name));
            if (!found) {
                Plugin plugin = loadedPlugins.get(name);
                plugin.setActivated(true);
                pluginService.save(plugin, plugin.getViewer().getMimeTypeIn());
                log.info("Plugin inserted: {}", plugin.getName());
            }
        }
    }
}
