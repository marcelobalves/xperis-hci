package pt.ulusofona.heilab.xperis.service;

import java.util.List;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class ServiceUtils {

    public static <T> PageImpl<T> convertToPage(List<T> result, Pageable pageable) {
        if (pageable != null && pageable.isPaged()) {
            final int start = (int) pageable.getOffset();
            final int end = Math.min((start + pageable.getPageSize()), result.size());
            return new PageImpl<>(result.subList(start, end), pageable, result.size());
        } else {
            return new PageImpl<>(result);
        }
    }
}
