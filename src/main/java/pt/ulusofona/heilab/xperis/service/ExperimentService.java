package pt.ulusofona.heilab.xperis.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pt.ulusofona.heilab.xperis.domain.Authority;
import pt.ulusofona.heilab.xperis.domain.Experiment;
import pt.ulusofona.heilab.xperis.domain.ExperimentMetaData;
import pt.ulusofona.heilab.xperis.domain.User;
import pt.ulusofona.heilab.xperis.domain.enumeration.ExperimentStatus;
import pt.ulusofona.heilab.xperis.repository.ExperimentDataRepository;
import pt.ulusofona.heilab.xperis.repository.ExperimentMetaDataRepository;
import pt.ulusofona.heilab.xperis.repository.ExperimentRepository;
import pt.ulusofona.heilab.xperis.repository.UserRepository;
import pt.ulusofona.heilab.xperis.security.AuthoritiesConstants;
import pt.ulusofona.heilab.xperis.security.SecurityUtils;
import pt.ulusofona.heilab.xperis.service.exception.NotAuthorizedException;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Experiment}.
 */
@Service
public class ExperimentService {

    private final Logger log = LoggerFactory.getLogger(ExperimentService.class);

    private final ExperimentRepository experimentRepository;
    private final ExperimentMetaDataRepository experimentMetaDataRepository;
    private final ExperimentDataRepository experimentDataRepository;
    private final UserRepository userRepository;

    public ExperimentService(
        ExperimentRepository experimentRepository,
        ExperimentMetaDataRepository experimentMetaDataRepository,
        ExperimentDataRepository experimentDataRepository,
        UserRepository userRepository
    ) {
        this.experimentRepository = experimentRepository;
        this.experimentMetaDataRepository = experimentMetaDataRepository;
        this.experimentDataRepository = experimentDataRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a experiment.
     *
     * @param experiment the entity to save.
     * @return the persisted entity.
     */
    public Experiment save(Experiment experiment) {
        if (experiment.getId() != null && !canUpdate(experiment)) {
            throw new NotAuthorizedException();
        }
        log.debug("Request to save Experiment : {}", experiment);
        return experimentRepository.save(experiment);
    }

    public void addExperimentMetaData(String experimentId, ExperimentMetaData experimentMetaData) {
        Experiment experiment = experimentRepository.findById(experimentId).get();
        experiment.addExperimentMetaData(experimentMetaData);
        experimentRepository.save(experiment);
    }
    public void removeExperimentMetaData(String experimentId, ExperimentMetaData experimentMetaData) {
        Experiment experiment = experimentRepository.findById(experimentId).get();
        experiment.removeExperimentMetaData(experimentMetaData);
        experimentRepository.save(experiment);
    }
    /**
     * Partially update a experiment.
     *
     * @param experiment the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Experiment> partialUpdate(Experiment experiment) {
        log.debug("Request to partially update Experiment : {}", experiment);
        return experimentRepository
            .findById(experiment.getId())
            .map(
                existingExperiment -> {
                    if (!canUpdate(existingExperiment)) {
                        throw new NotAuthorizedException();
                    }
                    if (experiment.getName() != null) {
                        existingExperiment.setName(experiment.getName());
                    }
                    if (experiment.getDescription() != null) {
                        existingExperiment.setDescription(experiment.getDescription());
                    }
                    if (experiment.getStartDate() != null) {
                        existingExperiment.setStartDate(experiment.getStartDate());
                    }
                    if (experiment.getFinishCaptureDate() != null) {
                        existingExperiment.setFinishCaptureDate(experiment.getFinishCaptureDate());
                    }
                    if (experiment.getFinishDate() != null) {
                        existingExperiment.setFinishDate(experiment.getFinishDate());
                    }
                    if (experiment.getMembers() != null) {
                        existingExperiment.setMembers(experiment.getMembers());
                    }
                    if (experiment.getAssistants() != null) {
                        existingExperiment.setAssistants(experiment.getAssistants());
                    }
                    if (experiment.getStatus() != null) {
                        existingExperiment.setStatus(experiment.getStatus());
                    }

                    Set<ExperimentMetaData> metaDataList = experimentMetaDataRepository.findAllByExperiment(existingExperiment.getId());
                    existingExperiment.setExperimentMetaData(metaDataList);

                    return existingExperiment;
                }
            )
            .map(experimentRepository::save);
    }

    /**
     * Get all the experiments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<Experiment> findAll(Pageable pageable) {
        User userLogged = getUserLogged();

        log.debug("Request to get all Experiments");
        List <String> ids = findAllIds();
        return experimentRepository.findAllInList(ids, pageable);
    }

    public List<String> findAllIds(){
        User userLogged = getUserLogged();
        List<Experiment> ids;
        if (userLoggedIsAdmin(userLogged)) {
            ids = experimentRepository.findAllIds();
        } else {
            if (userLoggedIsMaster(userLogged)) {
                ids = experimentRepository.findAllIdsForMembers(userLogged.getLogin(), userLogged.getId());
            } else {
                ids = experimentRepository.findAllIdsForViewers(userLogged.getId());
            }
        }
        return ids.stream().map(Experiment::getId).collect(Collectors.toList());
    }

    private User getUserLogged() {
        Optional<User> userLogged = Optional.empty();
        Optional<String> login = SecurityUtils.getCurrentUserLogin();
        if (login.isPresent()) {
            userLogged = userRepository.findOneByLogin(login.get());
            if (userLogged.isPresent()) return userLogged.get();
        }
        return userLogged.orElse(null);
    }

    /**
     * Get all the experiments with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<Experiment> findAllWithEagerRelationships(Pageable pageable) {
        return experimentRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one experiment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<Experiment> findOne(String id) {
        log.debug("Request to get Experiment : {}", id);
        Optional<Experiment> exp = experimentRepository.findOneWithEagerRelationships(id);
        if (hasPermission(exp, false)) {
            return exp;
        } else {
            return Optional.empty();
        }
    }

    /**
     * Delete the experiment by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        if (!canUpdate(id)) {
            throw new NotAuthorizedException();
        }
        log.debug("Request to delete Experiment : {}", id);
        experimentRepository.deleteById(id);
        experimentMetaDataRepository
            .findAllByExperiment(id)
            .forEach(
                experimentMetaData -> {
                    experimentMetaDataRepository.deleteById(experimentMetaData.getId());
                    experimentDataRepository.deleteAllByExperimentMetaData(experimentMetaData.getId());
                }
            );
    }

    public boolean hasPermission(Optional<Experiment> experiment, boolean toUpdate) {
        return hasPermission(experiment, toUpdate, getUserLogged());
    }

    public boolean hasPermission(Optional<Experiment> experiment, boolean toUpdate, User userLogged) {
        if (userLoggedIsAdmin(userLogged)) {
            return true;
        }
        if (experiment.isPresent()) {
            if (userLoggedIsMaster(userLogged)) {
                if (experiment.get().getId() == null) {
                    return true;
                }
                if (experiment.get().getCreatedBy() != null && experiment.get().getCreatedBy().equals(userLogged.getLogin())) {
                    return true;
                }
                return (
                    experiment.get().getMembers() != null &&
                    experiment.get().getMembers().contains(userLogged) &&
                    isValidPeriod(experiment.get())
                );
            } else {
                if (
                    experiment.get().getAssistants() != null &&
                    experiment.get().getAssistants().contains(userLogged) &&
                    isValidPeriod(experiment.get())
                ) {
                    return !toUpdate;
                }
            }
        }
        return false;
    }

    private boolean isValidPeriod(Experiment experiment) {
        return (
            experiment.getStatus().equals(ExperimentStatus.STARTED) || experiment.getStatus().equals(ExperimentStatus.CAPTURE_FINISHED)
        );
    }

    public boolean canUpdate(String experimentId) {
        return canUpdate(experimentRepository.findById(experimentId));
    }

    public boolean canUpdate(Experiment experiment) {
        return canUpdate(Optional.of(experiment));
    }

    public boolean canUpdate(Optional<Experiment> experiment) {
        return hasPermission(experiment, true);
    }

    private boolean userLoggedIsAdmin(User userLogged) {
        return userLogged.getAuthorities().contains(new Authority(AuthoritiesConstants.ADMIN));
    }

    private boolean userLoggedIsMaster(User userLogged) {
        return userLogged.getAuthorities().contains(new Authority(AuthoritiesConstants.MASTER));
    }
}
