package pt.ulusofona.heilab.xperis.service;

import java.util.List;
import java.util.Optional;
import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pt.ulusofona.heilab.xperis.domain.Plugin;
import pt.ulusofona.heilab.xperis.repository.DataMimeTypeRepository;
import pt.ulusofona.heilab.xperis.repository.PluginRepository;

/**
 * Service Implementation for managing {@link Plugin}.
 */
@Service
public class PluginService {

    private final Logger log = LoggerFactory.getLogger(PluginService.class);

    private final PluginRepository pluginRepository;
    private final DataMimeTypeRepository dataMimeTypeRepository;

    public PluginService(PluginRepository pluginRepository, DataMimeTypeRepository dataMimeTypeRepository) {
        this.pluginRepository = pluginRepository;
        this.dataMimeTypeRepository = dataMimeTypeRepository;
    }

    /**
     * Save a plugin.
     *
     * @param plugin the entity to save.
     * @return the persisted entity.
     */
    public Plugin save(Plugin plugin) {
        log.debug("Request to save Plugin : {}", plugin);
        return pluginRepository.save(plugin);
    }

    /**
     * Save a plugin.
     *
     * @param plugin the entity to save.
     * @param mimeType the mimeType field.
     * @return the persisted entity.
     */
    public Plugin save(Plugin plugin, String mimeType) {
        log.debug("Request to save Plugin : {}", plugin);
        plugin.setDataMimeType(dataMimeTypeRepository.findFirstByType(mimeType));
        return pluginRepository.save(plugin);
    }

    /**
     * Partially update a plugin.
     *
     * @param plugin the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Plugin> partialUpdate(Plugin plugin) {
        log.debug("Request to partially update Plugin : {}", plugin);

        return pluginRepository
            .findById(plugin.getId())
            .map(
                existingPlugin -> {
                    if (plugin.getName() != null) {
                        existingPlugin.setName(plugin.getName());
                    }
                    if (plugin.getClassName() != null) {
                        existingPlugin.setClassName(plugin.getClassName());
                    }
                    existingPlugin.setFileName(plugin.getFileName());
                    if (plugin.getDataMimeType() != null) {
                        existingPlugin.setDataMimeType(plugin.getDataMimeType());
                    }
                    return existingPlugin;
                }
            )
            .map(pluginRepository::save);
    }

    /**
     * Get all the plugins (pageable).
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<Plugin> findAll(Pageable pageable) {
        log.debug("Request to get all Plugins");
        return pluginRepository.findAll(pageable);
    }

    /**
     * Get all the active plugins.
     *
     * @param mimeTypeId the pagination information.
     * @return the list of entities.
     */
    public List<Plugin> findAll(String mimeTypeId) {
        log.debug("Request to get all Plugins");
        if (StringUtils.isEmpty(mimeTypeId)) {
            return pluginRepository.findAllByActivatedTrue();
        } else {
            return pluginRepository.findAllByDataMimeTypeAndActivatedTrue(mimeTypeId);
        }
    }

    /**
     * Get one plugin by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<Plugin> findOne(String id) {
        log.debug("Request to get Plugin : {}", id);
        return pluginRepository.findById(id);
    }

    /**
     * Delete the plugin by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete Plugin : {}", id);
        pluginRepository.deleteById(id);
    }
}
