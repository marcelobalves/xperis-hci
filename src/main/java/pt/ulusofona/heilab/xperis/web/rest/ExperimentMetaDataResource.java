package pt.ulusofona.heilab.xperis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ulusofona.heilab.xperis.domain.ExperimentMetaData;
import pt.ulusofona.heilab.xperis.repository.ExperimentMetaDataRepository;
import pt.ulusofona.heilab.xperis.security.AuthoritiesConstants;
import pt.ulusofona.heilab.xperis.service.ExperimentMetaDataService;
import pt.ulusofona.heilab.xperis.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link pt.ulusofona.heilab.xperis.domain.ExperimentMetaData}.
 */
@RestController
@RequestMapping("/api")
public class ExperimentMetaDataResource {

    private final Logger log = LoggerFactory.getLogger(ExperimentMetaDataResource.class);

    private static final String ENTITY_NAME = "experimentMetaData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ExperimentMetaDataService experimentMetaDataService;
    private final ExperimentMetaDataRepository experimentMetaDataRepository;

    public ExperimentMetaDataResource(
        ExperimentMetaDataService experimentMetaDataService,
        ExperimentMetaDataRepository experimentMetaDataRepository
    ) {
        this.experimentMetaDataService = experimentMetaDataService;
        this.experimentMetaDataRepository = experimentMetaDataRepository;
    }

    /**
     * {@code POST  /experiment-meta-data} : Create a new experimentMetaData.
     *
     * @param experimentMetaData the experimentMetaData to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new experimentMetaData, or with status {@code 400 (Bad Request)} if the experimentMetaData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/experiment-meta-data")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<ExperimentMetaData> createExperimentMetaData(@Valid @RequestBody ExperimentMetaData experimentMetaData)
        throws URISyntaxException {
        log.debug("REST request to save ExperimentMetaData : {}", experimentMetaData);
        if (experimentMetaData.getId() != null) {
            throw new BadRequestAlertException("A new experimentMetaData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ExperimentMetaData result = experimentMetaDataService.save(experimentMetaData);
        return ResponseEntity
            .created(new URI("/api/experiment-meta-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /experiment-meta-data/:id} : Updates an existing experimentMetaData.
     *
     * @param id the id of the experimentMetaData to save.
     * @param experimentMetaData the experimentMetaData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated experimentMetaData,
     * or with status {@code 400 (Bad Request)} if the experimentMetaData is not valid,
     * or with status {@code 500 (Internal Server Error)} if the experimentMetaData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/experiment-meta-data/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<ExperimentMetaData> updateExperimentMetaData(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody ExperimentMetaData experimentMetaData
    ) throws URISyntaxException {
        log.debug("REST request to update ExperimentMetaData : {}, {}", id, experimentMetaData);
        if (experimentMetaData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, experimentMetaData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!experimentMetaDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ExperimentMetaData result = experimentMetaDataService.save(experimentMetaData);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, experimentMetaData.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /experiment-meta-data/:id} : Partial updates given fields of an existing experimentMetaData, field will ignore if it is null
     *
     * @param id the id of the experimentMetaData to save.
     * @param experimentMetaData the experimentMetaData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated experimentMetaData,
     * or with status {@code 400 (Bad Request)} if the experimentMetaData is not valid,
     * or with status {@code 404 (Not Found)} if the experimentMetaData is not found,
     * or with status {@code 500 (Internal Server Error)} if the experimentMetaData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/experiment-meta-data/{id}", consumes = "application/merge-patch+json")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<ExperimentMetaData> partialUpdateExperimentMetaData(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody ExperimentMetaData experimentMetaData
    ) throws URISyntaxException {
        log.debug("REST request to partial update ExperimentMetaData partially : {}, {}", id, experimentMetaData);
        if (experimentMetaData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, experimentMetaData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!experimentMetaDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ExperimentMetaData> result = experimentMetaDataService.partialUpdate(experimentMetaData);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, experimentMetaData.getId())
        );
    }

    /**
     * {@code GET  /experiment-meta-data} : get all the experimentMetaData.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of experimentMetaData in body.
     */
    @GetMapping("/experiment-meta-data")
    public ResponseEntity<List<ExperimentMetaData>> getAllExperimentMetaData(Pageable pageable) {
        log.debug("REST request to get a page of ExperimentMetaData");
        Page<ExperimentMetaData> page = experimentMetaDataService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /experiment-meta-data/:id} : get the "id" experimentMetaData.
     *
     * @param id the id of the experimentMetaData to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the experimentMetaData, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/experiment-meta-data/{id}")
    public ResponseEntity<ExperimentMetaData> getExperimentMetaData(@PathVariable String id) {
        log.debug("REST request to get ExperimentMetaData : {}", id);
        Optional<ExperimentMetaData> experimentMetaData = experimentMetaDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(experimentMetaData);
    }

    /**
     * {@code DELETE  /experiment-meta-data/:id} : delete the "id" experimentMetaData.
     *
     * @param id the id of the experimentMetaData to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/experiment-meta-data/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.MASTER + "\")")
    public ResponseEntity<Void> deleteExperimentMetaData(@PathVariable String id) {
        log.debug("REST request to delete ExperimentMetaData : {}", id);
        experimentMetaDataService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    /**
     * {@code GET  /experiment-meta-data/:id/zip} : get all experiment datas into a zip file.
     *
     * @param id the id of the experimentMetaData to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body a zip file, or with status {@code 404 (Not Found)}.
     */
    @GetMapping(value = "/experiment-meta-data/{id}/zip", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getZipFile(@PathVariable String id) {
        log.debug("REST request to get Zip File from ExperimentMetaData : {}", id);
        byte[] zipFile = experimentMetaDataService.getZipFile(id);
        return zipFile;
    }

    /**
     * {@code GET  /experiment-meta-data/:id/plugin/:className} : get all experiment datas into a zip file.
     *
     * @param id the id of the experimentMetaData to retrieve.
     * @param className the name of plugin class
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body a file, or with status {@code 404 (Not Found)}.
     */
    @GetMapping(value = "/experiment-meta-data/{id}/plugin/{className}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getPluginView(@PathVariable String id, @PathVariable String className) {
        log.debug("REST request to get a FileViewer from ExperimentMetaData : {} by Plugin : {}", id, className);
        byte[] theFile = experimentMetaDataService.getPluginView(id, className);
        return theFile;
    }
}
