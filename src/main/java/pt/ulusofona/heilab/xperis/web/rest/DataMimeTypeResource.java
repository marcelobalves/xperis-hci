package pt.ulusofona.heilab.xperis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ulusofona.heilab.xperis.domain.DataMimeType;
import pt.ulusofona.heilab.xperis.repository.DataMimeTypeRepository;
import pt.ulusofona.heilab.xperis.service.DataMimeTypeService;
import pt.ulusofona.heilab.xperis.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link pt.ulusofona.heilab.xperis.domain.DataMimeType}.
 */
@RestController
@RequestMapping("/api")
public class DataMimeTypeResource {

    private final Logger log = LoggerFactory.getLogger(DataMimeTypeResource.class);

    private static final String ENTITY_NAME = "dataMimeType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DataMimeTypeService dataMimeTypeService;

    private final DataMimeTypeRepository dataMimeTypeRepository;

    public DataMimeTypeResource(DataMimeTypeService dataMimeTypeService, DataMimeTypeRepository dataMimeTypeRepository) {
        this.dataMimeTypeService = dataMimeTypeService;
        this.dataMimeTypeRepository = dataMimeTypeRepository;
    }

    /**
     * {@code POST  /data-mime-types/all} : Create dataMimeTypes from a list.
     *
     * @param dataMimeTypes list of dataMimeTypes to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dataMimeType, or with status {@code 400 (Bad Request)} if the dataMimeType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/data-mime-types/all")
    public ResponseEntity<List<DataMimeType>> createDataMimeType(@Valid @RequestBody List<DataMimeType> dataMimeTypes)
        throws URISyntaxException {
        log.debug("REST request to save DataMimeTypes : {}", dataMimeTypes.size());
        if (dataMimeTypes.size() == 0) {
            throw new BadRequestAlertException("A new dataMimeType cannot already have an ID", ENTITY_NAME, "idexists");
        }

        List<DataMimeType> result = dataMimeTypes.stream().map(dataMimeTypeService::save).collect(Collectors.toList());

        return ResponseEntity
            .created(new URI("/api/data-mime-types/"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, "1"))
            .body(result);
    }

    /**
     * {@code POST  /data-mime-types} : Create a new dataMimeType.
     *
     * @param dataMimeType the dataMimeType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dataMimeType, or with status {@code 400 (Bad Request)} if the dataMimeType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/data-mime-types")
    public ResponseEntity<DataMimeType> createDataMimeType(@Valid @RequestBody DataMimeType dataMimeType) throws URISyntaxException {
        log.debug("REST request to save DataMimeType : {}", dataMimeType);
        if (dataMimeType.getId() != null) {
            throw new BadRequestAlertException("A new dataMimeType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DataMimeType result = dataMimeTypeService.save(dataMimeType);
        return ResponseEntity
            .created(new URI("/api/data-mime-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /data-mime-types/:id} : Updates an existing dataMimeType.
     *
     * @param id the id of the dataMimeType to save.
     * @param dataMimeType the dataMimeType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dataMimeType,
     * or with status {@code 400 (Bad Request)} if the dataMimeType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dataMimeType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/data-mime-types/{id}")
    public ResponseEntity<DataMimeType> updateDataMimeType(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody DataMimeType dataMimeType
    ) throws URISyntaxException {
        log.debug("REST request to update DataMimeType : {}, {}", id, dataMimeType);
        if (dataMimeType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dataMimeType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dataMimeTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DataMimeType result = dataMimeTypeService.save(dataMimeType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dataMimeType.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /data-mime-types/:id} : Partial updates given fields of an existing dataMimeType, field will ignore if it is null
     *
     * @param id the id of the dataMimeType to save.
     * @param dataMimeType the dataMimeType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dataMimeType,
     * or with status {@code 400 (Bad Request)} if the dataMimeType is not valid,
     * or with status {@code 404 (Not Found)} if the dataMimeType is not found,
     * or with status {@code 500 (Internal Server Error)} if the dataMimeType couldn't be updated.
     */
    @PatchMapping(value = "/data-mime-types/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DataMimeType> partialUpdateDataMimeType(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody DataMimeType dataMimeType
    ) {
        log.debug("REST request to partial update DataMimeType partially : {}, {}", id, dataMimeType);
        if (dataMimeType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dataMimeType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dataMimeTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DataMimeType> result = dataMimeTypeService.partialUpdate(dataMimeType);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dataMimeType.getId())
        );
    }

    /**
     * {@code GET  /data-mime-types} : get all the dataMimeTypes with Paging.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dataMimeTypes in body.
     */
    @GetMapping("/data-mime-types")
    public ResponseEntity<List<DataMimeType>> getAllDataMimeTypes(Pageable pageable) {
        log.debug("REST request to get a page of DataMimeTypes");
        Page<DataMimeType> page = dataMimeTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /data-mime-types} : get all the dataMimeTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dataMimeTypes in body.
     */
    @GetMapping("/data-mime-types/all")
    public ResponseEntity<List<DataMimeType>> getAllDataMimeTypes() {
        log.debug("REST request to get a list of DataMimeTypes");
        Page<DataMimeType> page = dataMimeTypeService.findAll(Pageable.unpaged());
        return ResponseEntity.ok().body(page.getContent());
    }

    /**
     * {@code GET  /data-mime-types/:id} : get the "id" dataMimeType.
     *
     * @param id the id of the dataMimeType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dataMimeType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/data-mime-types/{id}")
    public ResponseEntity<DataMimeType> getDataMimeType(@PathVariable String id) {
        log.debug("REST request to get DataMimeType : {}", id);
        Optional<DataMimeType> dataMimeType = dataMimeTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dataMimeType);
    }

    /**
     * {@code DELETE  /data-mime-types/:id} : delete the "id" dataMimeType.
     *
     * @param id the id of the dataMimeType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/data-mime-types/{id}")
    public ResponseEntity<Void> deleteDataMimeType(@PathVariable String id) {
        log.debug("REST request to delete DataMimeType : {}", id);
        dataMimeTypeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
