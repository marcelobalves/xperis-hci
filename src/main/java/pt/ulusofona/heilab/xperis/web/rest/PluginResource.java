package pt.ulusofona.heilab.xperis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.ulusofona.heilab.xperis.domain.Plugin;
import pt.ulusofona.heilab.xperis.repository.PluginRepository;
import pt.ulusofona.heilab.xperis.service.PluginLoader;
import pt.ulusofona.heilab.xperis.service.PluginService;
import pt.ulusofona.heilab.xperis.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link Plugin}.
 */
@RestController
@RequestMapping("/api")
public class PluginResource {

    private final Logger log = LoggerFactory.getLogger(PluginResource.class);

    private static final String ENTITY_NAME = "plugin";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PluginService pluginService;
    private final PluginRepository pluginRepository;
    private final PluginLoader pluginLoader;

    public PluginResource(PluginService pluginService, PluginRepository pluginRepository, PluginLoader pluginLoader) {
        this.pluginService = pluginService;
        this.pluginRepository = pluginRepository;
        this.pluginLoader = pluginLoader;
    }

    /**
     * {@code POST  /plugins} : Create a new plugin.
     *
     * @param plugin the plugin to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new plugin, or with status {@code 400 (Bad Request)} if the plugin has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/plugins")
    public ResponseEntity<Plugin> createPlugin(@Valid @RequestBody Plugin plugin) throws URISyntaxException {
        log.debug("REST request to save Plugin : {}", plugin);
        if (plugin.getId() != null) {
            throw new BadRequestAlertException("A new plugin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Plugin result = pluginService.save(plugin);
        return ResponseEntity
            .created(new URI("/api/plugins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /plugins/:id} : Updates an existing plugin.
     *
     * @param id the id of the plugin to save.
     * @param plugin the plugin to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plugin,
     * or with status {@code 400 (Bad Request)} if the plugin is not valid,
     * or with status {@code 500 (Internal Server Error)} if the plugin couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/plugins/{id}")
    public ResponseEntity<Plugin> updatePlugin(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody Plugin plugin
    ) throws URISyntaxException {
        log.debug("REST request to update Plugin : {}, {}", id, plugin);
        if (plugin.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plugin.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!pluginRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Plugin result = pluginService.save(plugin);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plugin.getId()))
            .body(result);
    }

    /**
     * {@code PATCH  /plugins/:id} : Partial updates given fields of an existing plugin, field will ignore if it is null
     *
     * @param id the id of the plugin to save.
     * @param plugin the plugin to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated plugin,
     * or with status {@code 400 (Bad Request)} if the plugin is not valid,
     * or with status {@code 404 (Not Found)} if the plugin is not found,
     * or with status {@code 500 (Internal Server Error)} if the plugin couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/plugins/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Plugin> partialUpdatePlugin(
        @PathVariable(value = "id", required = false) final String id,
        @NotNull @RequestBody Plugin plugin
    ) throws URISyntaxException {
        log.debug("REST request to partial update Plugin partially : {}, {}", id, plugin);
        if (plugin.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, plugin.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!pluginRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Plugin> result = pluginService.partialUpdate(plugin);

        return ResponseUtil.wrapOrNotFound(result, HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, plugin.getId()));
    }

    /**
     * {@code GET  /plugins} : get all the plugins (pageable).
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plugins in body.
     */
    @GetMapping("/plugins")
    public ResponseEntity<List<Plugin>> getAllPlugins(Pageable pageable) {
        log.debug("REST request to get a page of Plugins");
        Page<Plugin> page = pluginService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /plugins/all} : get all the activated plugins.
     *
     * @param mimeTypeId the mimetype's id to retrieve activated plugins (Optional).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plugins in body.
     */
    @GetMapping("/plugins/all")
    public ResponseEntity<List<Plugin>> getAllPlugins(@RequestParam(required = false, value = "mimetype") String mimeTypeId) {
        log.debug("REST request to get a list of Plugins");
        List<Plugin> list = pluginService.findAll(mimeTypeId);
        return ResponseEntity.ok().body(list);
    }

    /**
     * {@code GET  /plugins/:id} : get the "id" plugin.
     *
     * @param id the id of the plugin to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the plugin, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/plugins/{id}")
    public ResponseEntity<Plugin> getPlugin(@PathVariable String id) {
        log.debug("REST request to get Plugin : {}", id);
        Optional<Plugin> plugin = pluginService.findOne(id);
        return ResponseUtil.wrapOrNotFound(plugin);
    }

    /**
     * {@code DELETE  /plugins/:id} : delete the "id" plugin.
     *
     * @param id the id of the plugin to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/plugins/{id}")
    public ResponseEntity<Void> deletePlugin(@PathVariable String id) {
        log.debug("REST request to delete Plugin : {}", id);
        pluginService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    /**
     * {@code POST  /plugins/load} : (re)load all the plugins in [plugin] folder .
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of plugins in body.
     */
    @PostMapping("/plugins/load")
    public ResponseEntity<List<Plugin>> loadPlugins() {
        log.debug("REST request to load Plugins");
        pluginLoader.load();
        List<Plugin> list = pluginService.findAll((String) null);
        return ResponseEntity.ok().body(list);
    }
}
