package pt.ulusofona.heilab.xperis.config;

import org.springframework.beans.PropertyAccessor;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;
import pt.ulusofona.heilab.xperis.service.DatabaseSequenceService;

@Component
public class DatabaseEventListener extends AbstractMongoEventListener<Object> {

    private DatabaseSequenceService sequenceGenerator;

    @Autowired
    public DatabaseEventListener(DatabaseSequenceService sequenceGenerator) {
        this.sequenceGenerator = sequenceGenerator;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Object> event) {
        PropertyAccessor myAccessor = PropertyAccessorFactory.forDirectFieldAccess(event.getSource());
        if (myAccessor.getPropertyType("id") == Long.class && myAccessor.getPropertyValue("id") == null) {
            myAccessor.setPropertyValue("id", sequenceGenerator.generateSequence(event.getSource().getClass().getSimpleName()));
        }
    }
}
