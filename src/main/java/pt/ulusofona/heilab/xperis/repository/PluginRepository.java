package pt.ulusofona.heilab.xperis.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.ulusofona.heilab.xperis.domain.Plugin;

/**
 * Spring Data MongoDB repository for the Plugin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PluginRepository extends MongoRepository<Plugin, String> {
    Page<Plugin> findAllByActivatedTrue(Pageable pageable);

    List<Plugin> findAllByActivatedTrue();

    List<Plugin> findAllByDataMimeTypeAndActivatedTrue(String dataMimeTypeId);
}
