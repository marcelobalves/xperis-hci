package pt.ulusofona.heilab.xperis.repository;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import pt.ulusofona.heilab.xperis.domain.Experiment;
import pt.ulusofona.heilab.xperis.domain.ExperimentMetaData;

import java.util.Set;

/**
 * Spring Data MongoDB repository for the ExperimentMetaData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExperimentMetaDataRepository extends MongoRepository<ExperimentMetaData, String> {
    Page<ExperimentMetaData> findAllByExperiment(Experiment experiment, Pageable pageable);

    Set<ExperimentMetaData> findAllByExperiment(String id);

    Page<ExperimentMetaData> findAllByExperimentIn(Set<Experiment> experiments, Pageable pageable);

    @Query(value = "{'experiment.$id': {$in: ?0} }")
    Page<ExperimentMetaData> findAllInExperiments(Set<ObjectId> experimentIds, Pageable pageable);

    void deleteAllByExperiment(String experimentId);
}
