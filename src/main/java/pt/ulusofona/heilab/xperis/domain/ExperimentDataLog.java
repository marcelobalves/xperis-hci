package pt.ulusofona.heilab.xperis.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import pt.ulusofona.heilab.xperis.domain.enumeration.LogAction;

/**
 * A ExperimentDataLog.
 */
@Document(collection = "experiment_data_log")
public class ExperimentDataLog extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("log_action")
    private LogAction logAction;

    @Field("experiment_data_id")
    private String experimentDataId;

    public ExperimentDataLog() {}

    public ExperimentDataLog(String experimentDataId, LogAction logAction) {
        this.experimentDataId = experimentDataId;
        this.logAction = logAction;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExperimentDataId() {
        return experimentDataId;
    }

    public void setExperimentDataId(String experimentDataId) {
        this.experimentDataId = experimentDataId;
    }

    public LogAction getLogAction() {
        return logAction;
    }

    public void setLogAction(LogAction logAction) {
        this.logAction = logAction;
    }
}
