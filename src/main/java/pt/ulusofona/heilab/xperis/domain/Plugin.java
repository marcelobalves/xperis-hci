package pt.ulusofona.heilab.xperis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import pt.ulusofona.heilab.xperis.service.spi.PluginViewer;

/**
 * A Plugin.
 */
@Document(collection = "plugin")
public class Plugin extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("name")
    private String name;

    @Field("class_name")
    private String className;

    @Field("file_name")
    private String fileName;

    private boolean activated = false;

    @DBRef
    @Field("dataMimeType")
    @JsonIgnoreProperties(value = { "experimentMetaData", "plugins" }, allowSetters = true)
    private DataMimeType dataMimeType;

    @Transient
    private PluginViewer viewer;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Plugin id(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Plugin name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return this.className;
    }

    public Plugin className(String className) {
        this.className = className;
        return this;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getFileName() {
        return this.fileName;
    }

    public Plugin fileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public DataMimeType getDataMimeType() {
        return this.dataMimeType;
    }

    public Plugin dataMimeType(DataMimeType dataMimeType) {
        this.setDataMimeType(dataMimeType);
        return this;
    }

    public void setDataMimeType(DataMimeType dataMimeType) {
        this.dataMimeType = dataMimeType;
    }

    public PluginViewer getViewer() {
        return viewer;
    }

    public void setViewer(PluginViewer viewer) {
        this.viewer = viewer;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Plugin)) {
            return false;
        }
        return id != null && id.equals(((Plugin) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Plugin{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", activated='" + isActivated() + "'" +
            ", className='" + getClassName() + "'" +
            ", fileName='" + getFileName() + "'" +
            "}";
    }
}
