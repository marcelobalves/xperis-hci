package pt.ulusofona.heilab.xperis.domain.enumeration;

/**
 * The RegisterType enumeration.
 */
public enum RegisterType {
    NO_REGISTER,
    TOKEN,
    SELF_REGISTER,
}
