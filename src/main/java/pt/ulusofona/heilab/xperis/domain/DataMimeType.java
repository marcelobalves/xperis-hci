package pt.ulusofona.heilab.xperis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DataMimeType.
 */
@Document(collection = "data_mime_type")
public class DataMimeType extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("type")
    private String type;

    @NotNull
    @Field("description")
    private String description;

    @NotNull
    @Field("file_extension")
    private String fileExtension;

    @DBRef(lazy = true)
    @Field("experimentMetaData")
    @JsonIgnoreProperties(value = { "experimentData", "experiment", "dataMimeType" }, allowSetters = true)
    private Set<ExperimentMetaData> experimentMetaData = new HashSet<>();

    @DBRef(lazy = true)
    @Field("dataTemplate")
    @JsonIgnoreProperties(value = { "dataMimeType" }, allowSetters = true)
    private Set<DataTemplate> dataTemplates = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DataMimeType id(String id) {
        this.id = id;
        return this;
    }

    public String getType() {
        return this.type;
    }

    public DataMimeType type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileExtension() {
        return this.fileExtension;
    }

    public DataMimeType fileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
        return this;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public Set<ExperimentMetaData> getExperimentMetaData() {
        return this.experimentMetaData;
    }

    public DataMimeType experimentMetaData(Set<ExperimentMetaData> experimentMetaData) {
        this.setExperimentMetaData(experimentMetaData);
        return this;
    }

    public DataMimeType addExperimentMetaData(ExperimentMetaData experimentMetaData) {
        this.experimentMetaData.add(experimentMetaData);
        experimentMetaData.setDataMimeType(this);
        return this;
    }

    public DataMimeType removeExperimentMetaData(ExperimentMetaData experimentMetaData) {
        this.experimentMetaData.remove(experimentMetaData);
        experimentMetaData.setDataMimeType(null);
        return this;
    }

    public void setExperimentMetaData(Set<ExperimentMetaData> experimentMetaData) {
        if (this.experimentMetaData != null) {
            this.experimentMetaData.forEach(i -> i.setDataMimeType(null));
        }
        if (experimentMetaData != null) {
            experimentMetaData.forEach(i -> i.setDataMimeType(this));
        }
        this.experimentMetaData = experimentMetaData;
    }

    public Set<DataTemplate> getDataTemplates() {
        return this.dataTemplates;
    }

    public DataMimeType dataTemplates(Set<DataTemplate> dataTemplates) {
        this.setDataTemplates(dataTemplates);
        return this;
    }

    public DataMimeType addDataTemplate(DataTemplate dataTemplate) {
        this.dataTemplates.add(dataTemplate);
        dataTemplate.setDataMimeType(this);
        return this;
    }

    public DataMimeType removeDataTemplate(DataTemplate dataTemplate) {
        this.dataTemplates.remove(dataTemplate);
        dataTemplate.setDataMimeType(null);
        return this;
    }

    public void setDataTemplates(Set<DataTemplate> dataTemplates) {
        if (this.dataTemplates != null) {
            this.dataTemplates.forEach(i -> i.setDataMimeType(null));
        }
        if (dataTemplates != null) {
            dataTemplates.forEach(i -> i.setDataMimeType(this));
        }
        this.dataTemplates = dataTemplates;
    }

    public String getDescription() {
        return description;
    }

    public DataMimeType description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DataMimeType)) {
            return false;
        }
        return id != null && id.equals(((DataMimeType) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DataMimeType{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", description='" + getDescription() + "'" +
            ", fileExtension='" + getFileExtension() + "'" +
            "}";
    }
}
